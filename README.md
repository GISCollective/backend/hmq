# HTTP Message Queue

GISCollective is an open source platform for easy map-making.

This repository contains the code for the message queue used by GISCollective. It is based on the [vibe.d](https://vibed.org/) web server and its used as a load balancer for messages passed to the tasks servers.

The `source/gis_collective/hmq/api/service.d` is the main entry point of the server. It defines the web server initialization. The server defines two custom routes:

* `PUT /subscribe` allows the task servers to subscribe to a channel:
  ```
    PUT /subscribe

    {
      "channel": "channel_name", // the channel name
      "url": "http://localhost:9095/channel_name", // the url where HMQ will forward messages from the channel
      "max": 10 // How many messages can be processed by the task server in the same time. This value is used to throttle received messages.
    }
  ```

* `PUT /unsubscribe` allows the task servers to unsubscribe from a channel:
  ```
    PUT /unsubscribe

    {
      "channel": "channel_name", // the channel name
      "url": "http://localhost:9095/channel_name", // the url where HMQ is currently forwarding messages
    }
  ```

Any POST request sent to a route is considered a message that needs to be forwarded, where the route is the channel name:

  ```
    POST /channel_name

    {
      /// any json object
    }
  ```

If the message has an `uid` field and there is already a message with the same value, it will be dropped.

This repository also contains a DLang client found in the `gis_collective.hmq.broadcast.http` module. It can be used by initializing a client and calling the push message:

```D
  import vibe.data.json;
  import gis_collective.hmq.broadcast.http;

  BroadcastConfig config;
  auto subscriber = new HttpPublisher(config, "channel_name");

  auto message = Json.emptyObject;
  subscriber.send(message);
```

Check the `examples` folder for a complete working sample.

You can read more about the architecture at [https://guide.giscollective.com/en/develop/architecture/](https://guide.giscollective.com/en/develop/architecture/) or find more about our platform at
 [https://giscollective.com](https://giscollective.com/).

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* A [DLang](https://dlang.org/) compiler. We recommend using [DMD](https://dlang.org/download.html) for development and [LDC](https://github.com/ldc-developers/ldc#installation) for production releases.
* The `openssl` and `libevent` libraries.

    For fedora/redhat:
    ```
      > dnf install -y openssl
    ```

    For ubuntu systems:
    ```
      > apt-get install -y git libssl-dev libevent-dev
    ```
* The [Trial](http://trial.szabobogdan.com/) test runner
* A mongo db server running

## Installation

* `git clone --recurse-submodules -j8 <repository-url>` this repository with its submodules
* `cd hmq`
* create an app `config/configuration.js` file based on `config/configuration.model.js`
* create a db `config/db` folder based on `config/db.model`
* make sure `mongo` db is running
* `dub`

## Running / Development

You can build and start the server by running `dub`.

### Writing Tests

We use [Trial](http://trial.szabobogdan.com/) for running tests and [FluentAsserts](http://fluentasserts.szabobogdan.com/) for writing assertions.

* `trial` to run all tests
* `trial -s "suite name"` to run just one suite
* `trial -t "test name"` to run tests that contain `test name` in the test name

### Building

We use [dub](https://dub.pm/commandline.html) for running and building this server:

* `dub` (development)
* `dub build --build release` (production)

For production environments we recommend building a docker container:

* `dub build --build release`
* `cp hmq deploy/service/hmq-fedora-36`
* `cd deploy`
* `docker build --build-arg APP_NAME=hmq --no-cache=true -t $IMAGE_TAG .`

### Deploying

This app can be deployed using `*.rpm`, `*.deb` packages, by downloading the artifacts from this repo, or using a container from https://gitlab.com/GISCollective/backend/hmq/container_registry/586064

## Contributing

You can find our contribution guide at [https://guide.giscollective.com/en/develop/CONTRIBUTING/](https://guide.giscollective.com/en/develop/CONTRIBUTING/)

## Code of conduct

You can find our code of conduct at [https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/](https://guide.giscollective.com/en/develop/CODE_OF_CONDUCT/)
