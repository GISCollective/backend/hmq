/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
import gis_collective.hmq.service;
import vibe.service.main;

int main(string[] args) {
  return mainService!(HmqService)(args);
}
