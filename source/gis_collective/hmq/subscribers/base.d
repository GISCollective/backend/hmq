/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.subscribers.base;

import vibe.data.json;

///
interface Subscriber {
  /// An unique subscriber id
  string id() @safe nothrow;

  /// Flag for the subscriber health
  bool isFaulty() @safe nothrow;

  /// Sends a json message to the subscriber
  bool send(Json);

  /// Send a short message to the subbscriber to check if it is alive
  void ping() @safe nothrow;
}
