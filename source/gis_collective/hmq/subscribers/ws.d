/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.subscribers.ws;

import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.log;
import gis_collective.hmq.envelope;

import vibe.http.websockets;
import vibe.data.json;
import vibe.core.core;

import std.datetime;

class WsSubscriber : Subscriber {

  private {
    WebSocket ws;
    string _id;
    void delegate(MQEnvelope) @safe onReceive;
  }

  this(WebSocket ws, string id, void delegate(MQEnvelope) @safe onReceive) @safe {
    this.ws = ws;
    this._id = id;

    runTask(() nothrow {
      try {
        while (ws.waitForData()) {
          MQEnvelope envelope;
          envelope.channel = ws.request.requestURI[1..$];
          envelope.time = Clock.currTime;
          envelope.data = ws.receiveText.parseJsonString;

          onReceive(envelope);
        }
      } catch(Exception e) {
        error(e);
      }
    });
  }

  string id() @safe nothrow {
    return _id;
  }

  bool send(Json data) @trusted nothrow {
    try {
      ws.send(data.to!string);
    } catch(Exception e) {
      error(e);
      return false;
    }

    return true;
  }

  bool isFaulty() nothrow @safe {
    return false;
  }

  void ping() nothrow @safe {

  }
}
