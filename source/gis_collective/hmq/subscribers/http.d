/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.subscribers.http;

import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.http_request;

import std.conv;

import vibe.http.client;
import vibe.data.json;
import vibe.core.log;
import vibe.stream.operations;
import vibe.service.stats;

class HttpSubscriber : Subscriber {

  private {
    HTTPClientSettings settings;
    string strUrl;
    bool _isFaulty;
  }

  this(string url) {
    this.strUrl = url;
    this.settings = new HTTPClientSettings;
  }

  string id() @safe nothrow {
    return strUrl;
  }

  bool send(Json data) @trusted nothrow {
    bool result;
    logDiagnostic("Sending POST HTTP request to `%s` server: %s", strUrl, data);

    try {
      auto url = URL(strUrl);
      auto code = post(url, data).code;
      _isFaulty = code == 0;

      result = code >= 200 && code < 300;

      Stats.instance.inc("Task", ["type": "http-push", "result": result ? "success" : "failure"]);
    } catch(Exception e) {
      logError("Failed to update http-push stats.");
    }

    return result;
  }

  bool isFaulty() nothrow @safe {
    return _isFaulty;
  }

  void ping() nothrow @safe {
    bool result;
    logDiagnostic("Ping `%s`", strUrl);

    try {
      auto url = URL(strUrl);
      auto response = get(url);

      _isFaulty = response.code != 200;
      logDiagnostic("Pong `%s` %s %s", strUrl, response.code, response.data);

      Stats.instance.inc("Subscriber", ["type": "ping", "result": _isFaulty ? "success" : "failure"]);
    } catch(Exception e) {
      logError("Failed to update http-push stats.");
      _isFaulty = false;
    }
  }
}
