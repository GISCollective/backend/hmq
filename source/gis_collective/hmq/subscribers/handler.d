/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.subscribers.handler;

import gis_collective.hmq.subscribers.base;

import vibe.data.json;

/// Suvbscriber used for in memory comunication
class HandlerSubscriber : Subscriber {

  private {
    bool _isFaulty;
    string _id;
    bool delegate(string, Json) nothrow handler;
  }

  this(string id, bool delegate(string url, Json data) nothrow handler = null) {
    this._id = id;
    this.handler = handler;
  }

  string id() @safe nothrow {
    return this._id;
  }

  bool send(Json data) {
    return this.handler(this._id, data);
  }

  bool isFaulty() nothrow @safe {
    return _isFaulty;
  }

  void isFaulty(bool value) nothrow @safe {
    _isFaulty = value;
  }

  void ping() nothrow @safe {

  }
}
