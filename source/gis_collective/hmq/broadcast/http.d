/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.broadcast.http;

import vibe.service.stats;

import std.conv;
import std.datetime;
import std.format;
import std.algorithm;
import std.exception;

import gis_collective.hmq.control.time_throttle;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.http_request;
import gis_collective.hmq.log;

import vibe.data.json;
import vibe.http.client;
import vibe.core.core;
import vibe.stream.operations;
import vibe.http.client;
import vibe.http.server;
import vibe.http.router;

version(unittest) {
  import fluent.asserts;
}

struct Response { int code; string data; }
/*
auto httpRequest(HTTPMethod method, URL url, Json data, HTTPClientSettings settings) @trusted nothrow {
  auto result = Response();

  try {
    auto cli = connectHTTP(url.host, url.port, false, settings);

    cli.request((scope req) {
      if (url.localURI.length) {
        assert(url.path.absolute, "Request URL path must be absolute.");
        req.requestURL = url.localURI;
      }

      req.method = method;
      req.writeJsonBody(data);
    }, (scope res) {
      result.code = res.statusCode;
      result.data = res.bodyReader.readAllUTF8();

      res.disconnect;
    });

    cli.disconnect;
  } catch(Exception e) {
    error(e);
  }

  return result;
}*/

alias HttpBroadcast = RemoteBroadcast!(HttpPublisher, HttpSubscriber);

class HttpSubscriber {
  private {
    BroadcastConfig config;
    string channel;
    IBroadcast.Subscriber subscriber;

    URL subscribeUrl;
    size_t max = 10;
    size_t count;

    HTTPClientSettings settings;
    Timer timer;

    SysTime lastPing;
    Duration pingInterval;

    bool isBusy;
  }

  this(BroadcastConfig config, string channel, IBroadcast.Subscriber subscriber) @safe {
    this.channel = channel;
    this.config = config;
    this.subscriber = subscriber;
    this.pingInterval = 60.seconds;

    auto segment = GenericPath!(vibe.core.path.InetPathFormat).Segment("subscribe");
    subscribeUrl = URL(config.hmqUrl);
    subscribeUrl.path = subscribeUrl.path ~ segment;

    auto timer = createTimer(&this.tryRegister);
    timer.rearm(pingInterval, true);

    config.router.post("/" ~ channel, &handler);
    config.router.get("/" ~ channel, &pingHandler);
  }

  size_t length() {
    return this.count;
  }

  void pingHandler(HTTPServerRequest req, HTTPServerResponse res) @safe nothrow {
    try {
      string interval = to!string(Clock.currTime - lastPing);

      this.ping;
      res.writeBody(`pong:` ~ interval, 200, "text/plain");
    } catch(Exception e) {
      error(e);
    }
  }

  void handler(HTTPServerRequest req, HTTPServerResponse res) @safe nothrow {
    this.count++;
    scope(exit) this.count--;

    try {
      subscriber(req.json);
      res.writeBody(`{}`, 200, "application/json");
    } catch(Exception e) {
      try {
        res.writeBody(`{}`, 400, "application/json");
        error(e);
      } catch(Exception) {}
    }

    this.ping;
  }

  void ping() @safe nothrow {
    lastPing = Clock.currTime;
  }

  void tryRegister() @safe nothrow {
    if(lastPing > Clock.currTime - pingInterval) {
      return;
    }

    register();
  }

  bool register() @safe nothrow {
    if(HttpRegistration.instance !is null) {
      HttpRegistration.instance.register(channel);
      return true;
    }

    info("[%s] Subscribing `%s` to `%s`.", channel, config.localUrl, subscribeUrl);

    try {
      auto data = Json.emptyObject;
      data["channel"] = channel;
      data["url"] = config.localUrl;
      data["max"] = max;

      auto code = put(subscribeUrl, data).code;
      return code >= 200 && code < 300;
    } catch(Exception e) {
      error(e);
      return false;
    }
  }
}

class HttpPublisher {

  private {
    URL url;
    HTTPClientSettings settings;
    IQueue!Json values;
    Duration pingInterval;
    BroadcastConfig config;
    string channel;

    bool sending;
  }

  this(BroadcastConfig config, string channel) @safe {
    this.config = config;
    this.channel = channel;

    auto segment = GenericPath!(vibe.core.path.InetPathFormat).Segment(channel);

    if(!config.hmqUrl.endsWith('/')) {
      config.hmqUrl = config.hmqUrl ~ "/";
    }

    url = URL(config.hmqUrl);
    url.path = url.path ~ segment;

    this.settings = new HTTPClientSettings;
    this.settings.defaultKeepAliveTimeout = 0.seconds;
    pingInterval = 2.seconds;

    auto timer = createTimer(&this.sendValues);
    timer.rearm(pingInterval, true);

    if(config.queueFactory is null) {
      values = new MemoryQueue!Json;
    } else {
      values = config.queueFactory(channel);
    }
  }

  size_t length() {
    return values.length;
  }

  void sendValues() @safe nothrow {
    if(this.config.errorThrottle.isFaulty || values is null) {
      return;
    }

    sending = true;
    scope(exit) sending = false;

    if(Stats.instance !is null) {
      Stats.instance.set("Broadcast", values.length, ["type": "push_storage", "action": "size", "channel": channel]);
    }

    while(!values.empty) {
      auto value = values.back;
      values.popBack;

      if(!send(value)) {
        values.put(value);
        this.config.errorThrottle.penalize;
        return;
      }

      this.config.errorThrottle.reset;
    }
  }

  bool send(Json value) @safe nothrow {
    auto begin = Clock.currTime;

    auto result = post(url, value);

    try {
      diagnostic("result code: %s", result.code);

      if(result.code == 0) {
        sleep(1.seconds);
      }
    } catch(Exception e) {
      error(e);
    }

    if(result.code >= 400) {
      error("HMQ at `%s` responded to `%s` with status `%s`:%s", url, value, result.code, result.data);
    }

    auto diff = Clock.currTime - begin;
    Stats.instance.avg("Broadcast", diff.total!"nsecs", ["type": "http", "action": "duration", "channel": channel]);

    if(result.code >= 200 && result.code < 300) {
      Stats.instance.inc("Broadcast", ["type": "http", "action": "success", "channel": channel]);
    } else {
      Stats.instance.inc("Broadcast", ["type": "http", "action": "failure", "channel": channel]);
    }

    return result.code >= 200 && result.code < 300;
  }

  void push(Json value) @safe nothrow {

    try {
      if(value.type != Json.Type.object) {
        error("You tried to HTTP push `" ~ value.to!string ~ "` of type `" ~ value.type.to!string ~ "`. Only objects are accepted.");
        return;
      }
    } catch(Exception e) {
      return;
    }

    values.put(value);
  }
}

class HttpRegistration {

  static HttpRegistration instance;

  private {
    BroadcastConfig config;
    string[] channels;
    size_t max = 10;
    URL subscribeUrl;
  }

  @safe nothrow {
    this(BroadcastConfig config) {
      this.config = config;

      try {
        auto segment = GenericPath!(vibe.core.path.InetPathFormat).Segment("subscribe");
        subscribeUrl = URL(config.hmqUrl);
        subscribeUrl.path = subscribeUrl.path ~ segment;
      } catch(Exception e) error(e);
    }

    string[] pendingRegistrations() {
      return channels;
    }

    void register(string channel) {
      if(channels.canFind(channel)) {
        return;
      }

      channels ~= channel;
    }

    void clear() {
      channels = [];
    }

    Json registrationMessage() {
      auto data = Json.emptyObject;

      try {
        data["channelList"] = channels.serializeToJson;
        data["url"] = config.localUrl;
        data["max"] = max;
      } catch(Exception err) error(err);

      return data;
    }
  }

  void flush() @safe {
    if(channels.length == 0) {
      return;
    }

    auto message = registrationMessage();
    auto response = put(subscribeUrl, message);

    if(response.code >= 200 && response.code < 300) {
      info("Successfully registered " ~ channels.length.to!string ~ " channels: " ~ channels.to!string);
      channels = [];
    } else {
      error("Could not register " ~ channels.length.to!string ~ " channels: " ~ channels.to!string);
    }
  }
}
