/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.broadcast.ws;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.queue.memory;

import vibe.http.websockets;
import vibe.data.json;
import vibe.inet.url;
import vibe.core.core;

alias WsBroadcast = RemoteBroadcast!(WsPublisher, WsSubscriber);

class WsConnection {
  protected {
    WebSocket ws;
    BroadcastConfig config;
    string channel;
  }

  this(BroadcastConfig config, string channel) @safe nothrow {
    this.config = config;
    this.channel = channel;
  }

  void wsLoop() @safe nothrow {}

  bool connect() @safe nothrow {
    try {
      auto ws_url = URL(config.hmqUrl ~ channel);
      this.ws = connectWebSocket(ws_url);
      info("WebSocket connected");

      runTask(() nothrow {
        try {
          wsLoop();
          this.ws.close;
        } catch(Exception e) {
          error(e);
        }
      });
    } catch(Exception e) {
      error(e);
      return false;
    }

    return true;
  }
}

class WsSubscriber : WsConnection {
  private {
    IBroadcast.Subscriber subscriber;
  }

  this(BroadcastConfig config, string channel, IBroadcast.Subscriber subscriber) @safe nothrow {
    super(config, channel);
    this.subscriber = subscriber;
  }

  override void wsLoop() {
    info("Starting subscriber task");
    scope(exit) info("Subscriber task finnished");

    try {
      while (ws.waitForData()) {
        auto txt = ws.receiveText;
        subscriber(txt.parseJsonString);
      }
    } catch(Exception e) {
      error(e);
    }
  }

  size_t length() {
    return 0;
  }

  bool register() @safe nothrow {
    return connect();
  }
}

class WsPublisher : WsConnection {
  IQueue!Json values;

  this(BroadcastConfig config, string channel) @safe nothrow {
    super(config, channel);

    if(config.queueFactory is null) {
      values = new MemoryQueue!Json;
    } else {
      values = config.queueFactory(channel);
    }
  }

  size_t length() {
    return values.length;
  }

  override void wsLoop() {
    try {
      while(ws.connected) {
        if(values.empty) {
          yield;
        } else {
          ws.send(values.back.to!string);
          values.popBack;
        }
      }
    } catch(Exception e) {
      error(e);
    }
  }

  bool push(const Json value) @safe nothrow {
    values.put(value);

    try {
      if(ws is null || !ws.connected) {
        connect();
      }

    } catch(Exception e) {
      error(e);
      return false;
    }

    return true;
  }
}
