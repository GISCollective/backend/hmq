/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.broadcast.memory;

import std.datetime;
import std.string;
import std.algorithm;
import std.range;

import vibe.service.stats;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.log;

import vibe.data.json;
import vibe.core.log;

version(unittest) {
  import fluent.asserts;
}

/// Message queue that stores data in the local memory
class MemoryBroadcast : IBroadcast {
  alias Subscriber = IBroadcast.Subscriber;

  private {
    Subscriber[][string] subscribers;
  }

  ///
  size_t queueSize() {
    return 0;
  }


  /// Register a subscriber to a channel
  bool register(const string channel, Subscriber subscriber) @safe nothrow {
    subscribers[channel] ~= subscriber;

    return true;
  }

  /// Push a message to channel. All subscribers registered to it, will receive it.
  bool push(const string channel, const Json value) @trusted nothrow {
    logDiagnostic("Got value: %s", value);
    Stats.instance.inc("Broadcast", ["type": "memory", "action": "push"]);

    if(channel !in subscribers) {
      error("There is no memory subscriber for the channel `" ~ channel ~ "`");
      return false;
    }

    try {
      foreach(subscriber; subscribers[channel]) {
        subscriber(value);
      }
    } catch (Exception e) {
      error(e);
      return false;
    }

    return true;
  }
}
