/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.broadcast.base;
import std.functional;

import gis_collective.hmq.log;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.control.time_throttle;

import vibe.data.json;
import vibe.http.router;

///
interface IBroadcast {
  ///
  alias Subscriber = void delegate(const Json) @safe;

  /// Register a subscriber to a channel
  bool register(const string channel, Subscriber subscriber) @safe nothrow;

  /// Push a new message to a channel
  bool push(const string channel, const Json value) @safe nothrow;

  /// Get the size of the local queue (for push and register)
  size_t queueSize();
}

/// Thrown when the requested resource could not be found but may be available in the future.
/// Subsequent requests by the client are permissible.
class BroadCastException : Exception {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);
  }

  Json toJson() {
    Json data = Json.emptyObject;
    data["errors"] = Json.emptyArray;
    data["errors"] ~= Json.emptyObject;

    data["errors"][0]["status"] = 500;
    data["errors"][0]["title"] = "Broadcast error";
    data["errors"][0]["description"] = message.dup;

    return data;
  }
}

struct BroadcastConfig {
  string hmqUrl;
  string localUrl;

  URLRouter router;
  TimeThrottle errorThrottle;
  IQueue!Json delegate(string) nothrow @safe queueFactory;
}

class RemoteBroadcast(Pub, Sub) : IBroadcast {
  private {
    BroadcastConfig config;
    Sub[string] subscribers;
    Pub[string] publishers;
  }

  this(BroadcastConfig config) @safe {
    this.config = config;

    if(this.config.errorThrottle is null) {
      this.config.errorThrottle = new TimeThrottle();
    }
  }

  ulong queueSize() {
    size_t total;

    foreach(key, val; subscribers) {
      total += val.length;
    }

    foreach(key, val; publishers) {
      total += val.length;
    }

    return total;
  }

  bool isFaulty() {
    return this.config.errorThrottle.isFaulty;
  }

  bool register(const string channel, IBroadcast.Subscriber subscriber) @safe nothrow {
    diagnostic("subscribe to `%s` on `%s`", config.hmqUrl, channel);

    try {
      subscribers[channel] = new Sub(config, channel, subscriber);
    } catch(Exception e) {
      error(e);
      return false;
    }

    return subscribers[channel].register;
  }

  bool push(const string channel, const Json value) @trusted nothrow {
    try {
      if(channel !in publishers) {
        publishers[channel] = new Pub(config, channel);
      }
    } catch(Exception e) {
      error(e);
      return false;
    }

    diagnostic("push to `%s`: `%s`", channel, value);
    publishers[channel].push(value);

    return true;
  }
}
