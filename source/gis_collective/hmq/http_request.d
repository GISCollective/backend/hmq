/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.http_request;

import std.conv;
import std.string;
import std.range;
import std.exception;
import std.datetime;
import std.algorithm;

import vibe.data.json;
import vibe.inet.url;
import vibe.core.net;
import vibe.core.core;
import vibe.stream.operations;
import vibe.internal.interfaceproxy : InterfaceProxy, interfaceProxy;

import gis_collective.hmq.log;

struct Response { int code; string data; }

Response get(URL url) @safe nothrow {
  return httpRequest!"GET"(url);
}

Response post(URL url, Json value) @safe nothrow {
  return httpRequest!"POST"(url, value);
}

Response put(URL url, Json value) @safe nothrow {
  return httpRequest!"PUT"(url, value);
}

Response httpRequest(string method)(URL url, Json value) @safe nothrow {
  Response response;
  TCPConnection connection;

  try {
    connection = connectTCP(url.host, url.port);
    connection.readTimeout(30.minutes);
  } catch(Exception e) {
    error("Can not send message to HMQ host at " ~ url.toString);
    return response;
  }

  scope(exit) connection.close;

  while(!connection.connected) {
    try {
      yield;
    } catch(Exception e) {
      error(e);
    }
  }

  try {
    string data = value.toPrettyString;
    string headers = method ~ " " ~ url.pathString ~ " HTTP/1.1\r\n";
    headers ~= "Host: " ~ url.host ~ "\r\n";
    headers ~= "Connection: close\r\n";
    headers ~= "Content-Type: application/json\r\n";

    ubyte[] rawData = data.representation.dup;
    headers ~= "Content-Length: " ~ rawData.length.to!string ~ "\r\n\r\n";

    connection.write(headers);
    connection.write(data);

    string[] responseLines = connection.readAllUTF8.split("\r\n");

    if(responseLines.length < 3) {
      return response;
    }

    string[] head = responseLines[0].split(" ");

    auto bodyStart = responseLines.countUntil("") + 1;
    response.code = head[1].to!int;
    response.data = responseLines[bodyStart..$].join("\r\n");
  } catch(Exception e) {
    error(e);
  }

  return response;
}


Response httpRequest(string method)(URL url) @safe nothrow {
  Response response;
  TCPConnection connection;

  try {
    connection = connectTCP(url.host, url.port);
    connection.readTimeout(30.minutes);
  } catch(Exception e) {
    error(e);
    return response;
  }

  scope(exit) connection.close;

  while(!connection.connected) {
    try {
      yield;
    } catch(Exception e) {
      error(e);
    }
  }

  try {
    string headers = method ~ " " ~ url.pathString ~ " HTTP/1.1\r\n";
    headers ~= "Host: " ~ url.host ~ "\r\n";
    headers ~= "Connection: close\r\n\r\n";

    connection.write(headers);

    string[] responseLines = connection.readAllUTF8.split("\r\n");

    if(responseLines.length < 3) {
      return response;
    }

    string[] head = responseLines[0].split(" ");

    auto bodyStart = responseLines.countUntil("") + 1;
    response.code = head[1].to!int;
    response.data = responseLines[bodyStart..$].join("\r\n");
  } catch(Exception e) {
    error(e);
  }

  return response;
}
