/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.queue.memory;

import std.conv;
import std.exception;
import std.datetime;
import gis_collective.hmq.queue.base;

version(unittest) {
  import vibe.data.json;
  import fluent.asserts;
  import gis_collective.hmq.envelope;
}

private class Node(T) {
  T value;
  Node prev;
  Node next;

  this(T value, Node prev = null, Node next = null) @safe @nogc nothrow {
    this.value = value;

    if(prev !is null) {
      prev.next = this;
    }

    this.prev = prev;
    this.next = next;

    if(next !is null) {
      next.prev = this;
    }
  }
}

class MemoryQueue(T) : IQueue!T {

  private {
    size_t count;
    Node!T root;
    Node!T tail;
  }

  void put(T newItem) @safe nothrow {
    static if(__traits(hasMember, T, "uid")) {
      if(this.exists(newItem.uid)) {
        return;
      }
    }

    count++;

    static if(__traits(hasMember, T, "time")) {
      for(auto node = root; node !is null; node = node.next) {
        if(node.value.time > newItem.time) {
          new Node!T(newItem, node.prev, node);
          return;
        }
      }
    }

    tail = new Node!T(newItem, tail, null);

    if(root is null) {
      root = tail;
    }
  }

  ref T front() @safe nothrow {
    return root.value;
  }

  ref T back() @safe nothrow {
    return tail.value;
  }

  void popFront() @safe @nogc nothrow {
    count--;
    root = root.next;

    if(root !is null) {
      root.prev = null;
    } else {
      tail = null;
    }
  }

  void popBack() @safe @nogc nothrow {
    count--;

    if(count > 0) {
      assert(tail.prev !is null, "tail.prev is null!");
    }

    tail = tail.prev;

    if(tail !is null) {
      tail.next = null;
    } else {
      root = null;
    }
  }

  size_t length() @safe @nogc nothrow {
    return count;
  }

  bool empty() @safe nothrow {
    return root is null;
  }

  static if(__traits(hasMember, T, "uid")) {
    bool exists(U)(U uid) @safe @nogc nothrow {
      if(count == 0 || uid == U.init) {
        return false;
      }

      auto node = root;

      while(node) {
        if(node.value.uid == uid) return true;

        node = node.next;
      }

      return false;
    }
  }
}

/// It should push an item to the queue
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.put(MQEnvelope(Json(1), Clock.currTime, ""));
  queue.front.data.should.equal(1);
  queue.back.data.should.equal(1);
}

/// It should push an item to the queue once when an item with the same uid exists
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.put(MQEnvelope(Json(1), Clock.currTime, "", "2"));
  queue.put(MQEnvelope(Json(2), Clock.currTime, "", "1"));
  queue.put(MQEnvelope(Json(2), Clock.currTime, "", "1"));

  queue.length.should.equal(2);
  queue.front.data.should.equal(1);
  queue.back.data.should.equal(2);
}

/// It should pop an item from the front queue
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.empty.should.equal(true);
  queue.put(MQEnvelope(Json(1), Clock.currTime, ""));
  queue.empty.should.equal(false);
  queue.popFront;
  queue.empty.should.equal(true);
}

/// It should pop an item from the back queue
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.empty.should.equal(true);
  queue.put(MQEnvelope(Json(1), Clock.currTime, ""));
  queue.empty.should.equal(false);
  queue.popBack;
  queue.empty.should.equal(true);
}

/// It should push an item sorted by time
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.put(MQEnvelope(Json(1), Clock.currTime - 1000.seconds));
  queue.put(MQEnvelope(Json(3), Clock.currTime));
  queue.put(MQEnvelope(Json(2), Clock.currTime - 500.seconds));

  queue.front.data.should.equal(1);
  queue.back.data.should.equal(3);

  queue.popFront;
  queue.front.data.should.equal(2);
  queue.back.data.should.equal(3);

  queue.popFront;
  queue.front.data.should.equal(3);
  queue.back.data.should.equal(3);
}


/// It should iterate on 3 elements added in order
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.put(MQEnvelope(Json(1), Clock.currTime - 2.minutes));
  queue.put(MQEnvelope(Json(2), Clock.currTime - 1.minutes));
  queue.put(MQEnvelope(Json(3), Clock.currTime - 10.seconds));

  queue.front.data.should.equal(1);
  queue.back.data.should.equal(3);
  queue.empty.should.equal(false);

  queue.popFront;
  queue.front.data.should.equal(2);
  queue.back.data.should.equal(3);
  queue.empty.should.equal(false);

  queue.popFront;
  queue.front.data.should.equal(3);
  queue.back.data.should.equal(3);
  queue.empty.should.equal(false);

  queue.popFront;
  queue.empty.should.equal(true);
}

/// It should iterate an unordered queue using pop back
unittest {
  auto queue = new MemoryQueue!MQEnvelope();

  queue.put(MQEnvelope(Json(1), Clock.currTime - 1000.seconds, ""));
  queue.put(MQEnvelope(Json(3), Clock.currTime, ""));
  queue.put(MQEnvelope(Json(2), Clock.currTime - 500.seconds, ""));

  queue.front.data.should.equal(1);
  queue.back.data.should.equal(3);

  queue.popBack;
  queue.front.data.should.equal(1);
  queue.back.data.should.equal(2);

  queue.popBack;
  queue.front.data.should.equal(1);
  queue.back.data.should.equal(1);
}
