/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.queue.overflow.fixed;

import gis_collective.hmq.queue.base;

class FixedOverflowRule : IOverflowRule {
  private size_t size;

  @safe nothrow {
    this(size_t size) {
      this.size = size;
    }

    bool isOverflow(size_t value) {
      return value > size;
    }

    size_t preferredSize() {
      return size;
    }
  }
}
