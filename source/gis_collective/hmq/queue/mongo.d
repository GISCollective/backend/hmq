/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.queue.mongo;

import gis_collective.hmq.queue.memory;
import gis_collective.hmq.log;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.queue.overflow.fixed;
import gis_collective.hmq.lifecycle;
import gis_collective.hmq.log;

import vibe.db.mongo.mongo;
import vibe.data.json;
import vibe.data.bson;
import vibe.core.log;

class MongoQueue(T) : MemoryQueue!T, IPersistentQueue!T {
  private {
    MongoCollection collection;
    IOverflowRule overflow;
    const string name;
  }

  this(MongoCollection collection) @safe nothrow {
    this.collection = collection;

    try {
      this.name = collection.name;
    } catch(Exception err) { error(err); }
    this.overflow = new FixedOverflowRule(1000);
    LifeCycle.instance.register(this);
  }

  override {
    void put(T newItem) @safe nothrow {
      if(overflow.isOverflow(length)) {
        info("Queue `%s` overflow. Storing in db.", name);
        store(newItem);
        return;
      }

      static if(__traits(hasMember, T, "uid")) {
        if(exists(newItem.uid)) {
          info("Message with uid=`%s` is already in queue.", newItem.uid);
          return;
        }
      }

      super.put(newItem);
    }

    ref T front() @safe nothrow {
      return super.front;
    }

    bool empty() @safe nothrow {
      if(super.empty) {
        this.restore;
      }

      return super.empty;
    }

    size_t overflowLength() @safe nothrow {
      try {
        return collection.countDocuments(Json.emptyObject);
      } catch(Exception e) {
        error(e);
        return 0;
      }
    }
  }

  void restore() @safe nothrow {
    long size;

    try {
      auto range = collection.find().limit(overflow.preferredSize);

      foreach(item; range) {
        size++;
        auto envelope = item.deserializeBson!T;
        collection.deleteOne(["_id": item["_id"]]);

        super.put(envelope);
      }
    } catch(Exception e) {
      error("Mongo store error:");
      error(e);
    }

    if(size > 0) {
      try {
        info("Mongo restored `%s` messages from `%s`", size, collection.name);
      } catch(Exception e) {
        error("Mongo store error:");
        error(e);
      }
    }
  }

  static if(__traits(hasMember, T, "uid")) {
    bool exists(U)(U uid) nothrow {
      if(super.exists(uid)) {
        return true;
      }

      return existsInDb(uid);
    }
  }

  static if(__traits(hasMember, T, "uid")) {
    bool existsInDb(U)(U uid) @safe nothrow {
      try {
        if(uid == U.init) {
          return false;
        }

        return collection.countDocuments([ "uid": uid ]) > 0;
      } catch(Exception e) {
        error("Mongo store error:");
        error(e);
      }

      return false;
    }
  }

  void store(T value) @safe nothrow {
    try {
      static if(__traits(hasMember, T, "uid")) {
        if(this.existsInDb(value.uid)) {
          return;
        }
      }

      collection.insertOne(value);
    } catch(Exception e) {
        error("Mongo store error: %s", value);
        error(e);
    }
  }

  void storeAll() @safe nothrow {
    info("There are %s items in memory.", super.length);

    while(!super.empty) {
      auto item = super.front;
      this.store(item);
      super.popFront;
    }
  }

  void setOverflowRule(IOverflowRule value) nothrow @safe {
    this.overflow = value;
  }

  IOverflowRule overflowRule() nothrow @safe {
    return this.overflow;
  }
}
