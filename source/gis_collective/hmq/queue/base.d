/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.queue.base;

import vibe.data.json : Json;

interface IQueue(T) {
  @safe nothrow {
    void put(T newItem);
    ref T front();
    ref T back();
    void popFront();
    void popBack();

    size_t length();

    bool empty();
  }
}

interface IPersistentQueue(T) : IQueue!T {
  @safe nothrow {
    void restore();
    void store(T value);
    void storeAll();

    size_t overflowLength();
    void setOverflowRule(IOverflowRule);
    IOverflowRule overflowRule();
  }
}

interface IOverflowRule {
  @safe nothrow {
    bool isOverflow(size_t value);
    size_t preferredSize();
  }
}

IQueue!Json defaultQueueFactory(string) {
  import gis_collective.hmq.queue.memory;

  return new MemoryQueue!Json;
}
