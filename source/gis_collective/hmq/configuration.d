/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.configuration;

import std.file;
import std.path;
import std.exception;
import std.conv;
import std.datetime;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.data.json;

import vibe.service.configuration.general;
import vibe.service.configuration.http;

/// The service configuration
struct MainApiConfiguration {
  /// Configurations for all services
  GeneralConfig general;

  /// The http configuration
  HTTPConfig http;
}
