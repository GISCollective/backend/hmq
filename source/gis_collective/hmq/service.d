/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.service;

import gis_collective.hmq.lifecycle;
import gis_collective.hmq.configuration;
import gis_collective.hmq.api;
import gis_collective.hmq.envelope;

import vibe.service.base;
import vibe.service.webservice;
import vibe.service.configuration.http;
import vibe.service.configuration.db;
import vibe.service.stats;

import core.thread;
import vibe.service.stats;

import std.exception;
import std.conv;

import vibe.core.core;
import vibe.core.log;
import vibe.db.mongo.mongo;
import vibe.http.server;
import vibe.http.client;
import vibe.http.router;
import vibe.stream.operations;

///
class HmqService : WebService!MainApiConfiguration {
  void shutdown() nothrow {
    logInfo("Shutting down the LifeCycle...");
    LifeCycle.instance.shutdown;
  }

  /// The main service logic
  int main() {
    scope(exit) this.statsTimer();

    auto router = new URLRouter;
    router.setupStats();

    log("Added stats routes.");

    auto db = this.configuration.general.dbConfiguration.getMongoDb;

    logInfo("Using `%s` mongo db", db.name);
    log("Db connection ready.");

    router.setupApi(db, this.configuration);
    log("Api setup ready.");

    auto config = this.configuration.http.toVibeConfig;
    config.serverString = this.info.name;

    logInfo("The maximum accepted request size is %s kb", config.maxRequestSize / 1024);
    logInfo("The server string is `%s`", config.serverString);

    listenHTTP(config, router);

    return runEventLoop();
  }
}
