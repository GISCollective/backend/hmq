/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.log;

import vibe.core.log;

void diagnostic(T...)(T args) @trusted nothrow {
  try {
    logDiagnostic(args);
  } catch (Exception) {}
}

void info(T...)(T args) @trusted nothrow {
  try {
    logInfo(args);
  } catch (Exception) {}
}

void error(T...)(lazy T args) @trusted nothrow {
  try {
    logError(args);
  } catch (Exception) {}
}

void error(Exception e) @trusted nothrow {
  try {
    debug {
      error(e.toString);
    } else {
      error(e.message);
    }
  } catch (Exception) {}
}
