/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.http_push_loop;

import std.datetime;
import std.stdio;
import std.algorithm;
import std.array;
import std.conv;
import std.exception;

import vibe.service.stats;

import gis_collective.hmq.round_robin;
import gis_collective.hmq.configuration;
import gis_collective.hmq.envelope;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.control.lock_collection;
import gis_collective.hmq.control.time_throttle;

import vibe.data.json;
import vibe.http.server;
import vibe.core.core;
import vibe.core.log;

/// Implements an loop that pushes message to subscribers over http
class HttpPushLoop {
  alias Handler = bool delegate(string url, Json data) nothrow;

  private {
    static {
      size_t requestCount;
      SysTime lastReport;
    }

    IQueue!MQEnvelope queue;
    IPersistentQueue!MQEnvelope persistentQueue;

    Handler handler;
    RoundRobin!Subscriber subscribers;
    LockCollection!string locks;
    TimeThrottle lockThrottle;

    long subscriberIndex;

    bool isRunning;
    string channel;
  }

  ///
  this(IQueue!MQEnvelope queue, RoundRobin!Subscriber subscribers, Handler handler, string channel) @trusted {
    this.queue = queue;
    this.persistentQueue = cast(IPersistentQueue!MQEnvelope) queue;

    this.locks = new LockCollection!string;
    this.subscribers = subscribers;
    this.handler = handler;
    this.channel = channel;

    ThrottleConfiguration config;
    config.penalizationMs = 10;
    config.penalizationTotalMs = 5000;
    config.errorLimit = size_t.max;

    this.lockThrottle = new TimeThrottle(config);

    Stats.instance.help("hmq_push_loop_duration", "The average duration in nanoseconds of how long it takes to process the queued messages");
    Stats.instance.help("hmq_push_duration", "The duration in nanoseconds of how long it takes to push the a message");
    Stats.instance.help("hmq_push_loop_requests", "The number of push messages sent in the last push loop");
    Stats.instance.help("hmq_push_errors", "The number of push errors");
    Stats.instance.help("hmq_push_success", "The number of push success");
    Stats.instance.help("hmq_push_troughput", "The number of request that were pushed in one second");
    Stats.instance.help("hmq_recycled_messages", "The number of messages pushed back to the queue, because of some push failure or a lock");
    Stats.instance.help("hmq_locked_uuid", "The number of locked locked uuids");
  }

  /// Check if there is data in the queue
  bool hasData() @trusted nothrow {
    return !queue.empty;
  }

  /// Check if the http push loop is ready
  bool isReady() @safe nothrow {
    return !isRunning && !isBusy && hasData && !lockThrottle.isFaulty;
  }

  /// Check if all subscribers are busy
  bool isBusy() @safe nothrow {
    return subscribers.isBusy;
  }

  /// Check if the loop is running
  bool isStarted() @safe nothrow {
    return isRunning;
  }

  /// Start the http push loop
  void start() @trusted {
    size_t push_count;

    if(subscribers.isBusy || !hasData) {
      logDebug("not ready.");
      return;
    }

    isRunning = true;
    auto begin = Clock.currTime;

    scope(exit) {
      Stats.instance.set("hmq_channel_size", queue.length, ["channel": channel]);

      if(persistentQueue !is null) {
        Stats.instance.set("hmq_channel_overflow_size", persistentQueue.overflowLength, ["channel": channel]);
      }

      Stats.instance.set("hmq_channel_active", 0, ["channel": channel]);

      if(isRunning) {
        auto diff = Clock.currTime - begin;
        Stats.instance.set("hmq_push_loop_duration", diff.total!"nsecs", ["channel": channel]);
        Stats.instance.set("hmq_push_loop_requests", push_count, ["channel": channel]);
      }

      isRunning = false;
    }

    size_t oldIndex;

    while(!subscribers.isBusy && hasData) {
      push_count++;
      Stats.instance.set("hmq_channel_active", 1, ["channel": channel]);
      logDebug("Pushing item.");

      auto subscriber = subscribers.next;
      if(oldIndex < subscriber.index) {
        yield;
      }

      oldIndex = subscriber.index;

      Stats.instance.set("hmq_instance_load", subscribers.usage(subscriber.index), ["subscriber_index": subscriber.index.to!string, "channel": channel]);
      Stats.instance.set("hmq_channel_size", queue.length, ["channel": channel]);

      if(persistentQueue !is null) {
        Stats.instance.set("hmq_channel_overflow_size", persistentQueue.overflowLength, ["channel": channel]);
        Stats.instance.set("hmq_channel_overflow_size", persistentQueue.overflowLength, ["channel": channel]);
      }

      if(queue.empty) {
        logInfo("the queue is empty");
        break;
      }

      auto item = queue.front;

      if(locks.isEnabled(item.uid)) {
        if(this.persistentQueue !is null) {
          this.persistentQueue.store(item);
          queue.popFront;
        }

        this.lockThrottle.penalize();
        continue;
      }

      locks.enable(item.uid);
      Stats.instance.set("hmq_locked_uuid", locks.count, ["channel": channel]);

      scope(exit) {
        locks.disable(item.uid);
        this.lockThrottle.reset;
        Stats.instance.set("hmq_locked_uuid", locks.count, ["channel": channel]);
      }

      debug auto before = queue.length;
      queue.popFront;
      debug assert(before > queue.length, "The queue item was not removed.");

      subscribers.reserve(subscriber.index);
      runTask(&this.handleItem, subscriber.index, item.serializeToJson.clone);

      auto diff = Clock.currTime - begin;
      Stats.instance.set("hmq_push_loop_duration", diff.total!"nsecs", ["channel": channel]);
      Stats.instance.set("hmq_instance_load", subscribers.usage(subscriber.index), ["subscriber_index": subscriber.index.to!string, "channel": channel]);
      Stats.instance.set("hmq_push_loop_requests", push_count, ["channel": channel]);
    }
  }

  /// Send an item to a subscriber
  private void handleItem(size_t index, Json localItem) nothrow {
    requestCount++;

    if(Clock.currTime - lastReport > 1.seconds) {
      lastReport = Clock.currTime;
      Stats.instance.avg("hmq_push_troughput", requestCount, ["channel": channel]);
      requestCount = 0;
    }

    auto subscriber = subscribers[index];

    auto begin = Clock.currTime;
    logDiagnostic("Sending `%s` to subscriber `%s` at `%s`.", localItem["data"].to!string, index, subscriber);

    try {
      bool success = subscriber.send(localItem["data"]);

      if(success) {
        Stats.instance.inc("hmq_push_success", ["channel": channel, "subscriber_index": index.to!string]);
        subscribers.reward(index);
      } else {
        Stats.instance.inc("hmq_push_errors", ["channel": channel, "subscriber_index": index.to!string]);
        logError("Could not send the data to worker for channel `%s`.", channel);
        subscribers.penalize(index);
        queue.put(localItem.deserializeJson!MQEnvelope);
        Stats.instance.inc("hmq_recycled_messages", ["channel": channel]);
      }

      subscribers.free(index);
    } catch(Exception e) {
      try { logError(e.toString); } catch(Exception) {}
    }

    auto diff = Clock.currTime - begin;
    Stats.instance.avg("hmq_push_duration", diff.total!"nsecs", ["channel": channel, "subscriber_index": index.to!string]);
  }
}
