/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.http_subscriber;

import std.datetime;
import std.conv;
import vibe.service.stats;

import gis_collective.hmq.configuration;
import gis_collective.hmq.envelope;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.subscribers.http;
import gis_collective.hmq.subscribers.ws;

import vibe.core.core;
import vibe.data.json;
import vibe.http.server;
import vibe.http.websockets;
import vibe.service.stats;

class HttpSubscribe {

  private {
    void delegate(string, Subscriber, size_t) @safe onSubscribe;
    void delegate(string, string) @safe onUnsubscribe;
    void delegate(MQEnvelope) @safe onReceive;

    size_t wsIndex;
  }

  this(void delegate(string, Subscriber, size_t) @safe onSubscribe, void delegate(string, string) @safe onUnsubscribe, void delegate(MQEnvelope) @safe onReceive) {
    this.onSubscribe = onSubscribe;
    this.onUnsubscribe = onUnsubscribe;
    this.onReceive = onReceive;
  }

  void subscribe(HTTPServerRequest req, HTTPServerResponse res) {
    Json json;

    try {
      json = req.json;
    } catch(BroadCastException e) {
      auto error = e.toJson;
      res.writeBody(error.toString, error["errors"]["status"].to!int, "application/json");
      return;
    }

    struct Value {
      string url;
      size_t max;

      @optional {
        string channel;
        string[] channelList;
      }
    }

    try {
      auto value = json.deserializeJson!Value;

      if(value.channelList.length == 0 && value.channel != "") {
        value.channelList ~= value.channel;
      }

      if(value.channelList.length == 0) {
        res.writeBody( `{ "errors": [{ "description": "You must provide a 'channel' or 'channelList'.", "status": 400, "title": "Invalid channel"}]}`, 400, "application/json");
        return;
      }

      foreach (channel; value.channelList) {
        Subscriber subscriber = new HttpSubscriber(value.url ~ channel);
        onSubscribe(channel, subscriber, value.max);
      }

      res.writeBody(`{}`, 200, "application/json");
    } catch(BroadCastException e) {
      auto error = e.toJson;
      res.writeBody(error.toString, error["errors"]["status"].to!int, "application/json");
    } catch(Exception e) {
      res.writeBody( `{ "errors": [{ "description": "` ~ e.message.idup ~ `", "status": 400, "title": "Parse error"}]}`, 400, "application/json");
    }
  }

  void unsubscribe(HTTPServerRequest req, HTTPServerResponse res) {
    Json json;

    try {
      json = req.json;
    } catch(BroadCastException e) {
      auto error = e.toJson;

      res.writeBody(error.toString, error["errors"]["status"].to!int, "application/json");
      return;
    }

    struct Value {
      string channel;
      string url;
    }

    try {
      auto value = json.deserializeJson!Value;
      onUnsubscribe(value.channel, value.url);
      res.writeBody(`{}`, 200, "application/json");
    } catch(BroadCastException e) {
      auto error = e.toJson;
      res.writeBody(error.toString, error["errors"]["status"].to!int, "application/json");
      return;
    }
  }

  void wsHandler(scope WebSocket sock) {
    wsIndex++;
    immutable wsId = "WS" ~ wsIndex.to!string;
    immutable channel = sock.request.requestURI[1..$];

    scope subscriber = new WsSubscriber(sock, wsId, onReceive);
    onSubscribe(channel, subscriber, 1);

    while (sock.connected) {
      sleep(1.seconds);
    }

    onUnsubscribe(channel, wsId);
  }
}
