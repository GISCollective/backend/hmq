/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.api;

import gis_collective.hmq.control.time_throttle;
import gis_collective.hmq.round_robin;
import gis_collective.hmq.http_subscriber;
import gis_collective.hmq.http_receiver;
import gis_collective.hmq.configuration;
import gis_collective.hmq.push_collection;
import gis_collective.hmq.envelope;

import vibe.service.stats;

import vibe.http.server;
import vibe.http.websockets;
import vibe.http.router;
import vibe.db.mongo.database;

///
void setupApi(URLRouter router, MongoDatabase db, MainApiConfiguration config) {
  auto pushCollection = new PushCollection(db, ThrottleConfiguration.init);
  auto receiver = new HttpReceiver(&pushCollection.push);
  auto httpSubscribe = new HttpSubscribe(&pushCollection.subscribe, &pushCollection.unsubscribe, &pushCollection.push);

  router.post("*", &receiver.push);

  router.put("/subscribe", &httpSubscribe.subscribe);
  router.put("/unsubscribe", &httpSubscribe.unsubscribe);

  router.get("*", handleWebSockets(&httpSubscribe.wsHandler));
}
