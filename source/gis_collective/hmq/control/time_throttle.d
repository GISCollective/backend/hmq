/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.control.time_throttle;

import vibe.core.log;
import std.datetime;

version(unittest) {
  import fluent.asserts;
}

///
struct ThrottleConfiguration {
  /// Time in ms for how long an item will be ignored in case of failure
  size_t penalizationMs = 500;

  /// The maximum time for how much an item can be penalized
  size_t penalizationTotalMs = 10000;

  /// The number of allowed errors before enter in faulty state
  size_t errorLimit = 0;
}

class TimeThrottle {
  private {
    immutable ThrottleConfiguration config;

    long penalizationCount;
    SysTime nextCall;
  }

  @safe nothrow:
    this() {
      this(ThrottleConfiguration());
    }
    this(const ThrottleConfiguration config) {
      this.config = config;
    }

    long penalizations() {
      return penalizationCount;
    }

    void penalize() {
      penalizationCount++;

      if(penalizationCount < config.errorLimit) {
        return;
      }

      auto now = Clock.currTime;
      auto counter = penalizationCount - config.errorLimit;

      auto interval = config.penalizationMs * counter;

      if(interval > config.penalizationTotalMs) {
        interval = config.penalizationTotalMs;
      }

      nextCall = now + interval.msecs;
    }

    bool isFaulty() {
      immutable now = Clock.currTime;
      return now < nextCall;
    }

    void resetTime() {
      nextCall = Clock.currTime;
    }

    void reset() {
      nextCall = Clock.currTime;
      penalizationCount = 0;
    }
}

/// TimeThrottle should not be faulty by default
unittest {
  auto throttle = new TimeThrottle(ThrottleConfiguration());
  throttle.isFaulty.should.equal(false);
}

/// TimeThrottle should not be faulty when the limit is reached
unittest {
  auto throttle = new TimeThrottle(ThrottleConfiguration());

  throttle.penalize();
  throttle.isFaulty.should.equal(true);
}

/// TimeThrottle should not be faulty when the limit is not reached
unittest {
  ThrottleConfiguration config;
  config.errorLimit = 2;

  auto throttle = new TimeThrottle(config);

  throttle.penalize();
  throttle.isFaulty.should.equal(false);
}
