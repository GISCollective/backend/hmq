/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.control.lock_collection;

version(unittest) {
  import fluent.asserts;
}

///
class LockCollection(T) {
  private {
    static Object item;
    Object[T] locks;
    size_t _count;
  }

  static this() {
    item = new Object;
  }

  ///
  void enable(const T key) @safe nothrow {
    if(isEnabled(key) || key == T.init) return;

    _count++;
    locks[key] = item;
  }

  ///
  void disable(const T key) @safe @nogc nothrow {
    if(!isEnabled(key) || key == T.init) return;

    _count--;
    locks.remove(key);
  }

  ///
  bool isEnabled(const T key) @safe @nogc nothrow {
    return (key in locks) !is null;
  }

  ///
  size_t count() @safe @nogc nothrow {
    return _count;
  }
}

/// It should have no locked keys on init
unittest {
  auto lock = new LockCollection!string;

  lock.count.should.equal(0);
}

/// It should have a locked key on after a lock
unittest {
  auto lock = new LockCollection!string;

  lock.enable("test1");
  lock.count.should.equal(1);
}

/// Locking a key twice should be counted once
unittest {
  auto lock = new LockCollection!string;

  lock.enable("test1");
  lock.enable("test1");
  lock.count.should.equal(1);
}

/// It should not lock the T.init value
unittest {
  auto lock = new LockCollection!string;

  lock.enable("");
  lock.count.should.equal(0);
}

/// Should not decrease the counter for unlocked keys
unittest {
  auto lock = new LockCollection!string;

  lock.disable("test1");
  lock.disable("test1");
  lock.count.should.equal(0);
}

/// Unlocking a key should unincrement the counnter
unittest {
  auto lock = new LockCollection!string;

  lock.enable("test1");
  lock.disable("test1");
  lock.count.should.equal(0);
}

/// It should be able to lock using a string key
unittest {
  auto lock = new LockCollection!string;

  lock.enable("test1");
  lock.isEnabled("test1").should.equal(true);
}

/// It should not lock keys by default
unittest {
  auto lock = new LockCollection!string;

  lock.isEnabled("test1").should.equal(false);
}

/// It should unlock a locked key
unittest {
  auto lock = new LockCollection!string;

  lock.enable("test1");
  lock.disable("test1");
  lock.isEnabled("test1").should.equal(false);
}

/// It should unlock an unlocked key
unittest {
  auto lock = new LockCollection!string;

  lock.disable("test1");
  lock.isEnabled("test1").should.equal(false);
}