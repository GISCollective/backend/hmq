/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.envelope;

import std.datetime;
import vibe.data.json;

struct MQEnvelope {
  Json data;
  @optional SysTime time;
  string channel;
  @optional string uid;
}
