/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.lifecycle;

import gis_collective.hmq.queue.base;
import gis_collective.hmq.log;

import vibe.core.log;

static this() {
  LifeCycle.instance = new LifeCycle;
}

class LifeCycle {

  private {
    void delegate() nothrow[] events;
    bool shuttingDown;
  }

  static LifeCycle instance;

  bool isShuttingDown() {
    return shuttingDown;
  }

  void register(T)(IPersistentQueue!T queue) {
    void storeAll() nothrow {
      queue.storeAll;
    }

    events ~= &storeAll;

    info("IPersistentQueue!" ~ T.stringof ~ " registered for shut down.");
  }

  void shutdown() nothrow {
    shuttingDown = true;
    info("[ SHUTDOWN ]");
    info("Running hmq shut down events...");

    foreach(event; events) {
      event();
    }

    info("Done running hmq shut down events...");
  }
}
