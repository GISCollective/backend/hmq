/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.push_collection;

import std.conv;
import std.datetime;

import vibe.service.stats;

import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import vibe.stream.operations;
import vibe.db.mongo.database;

import gis_collective.hmq.round_robin;
import gis_collective.hmq.envelope;
import gis_collective.hmq.http_push_loop;
import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.log;
import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.control.time_throttle;

/// Sends messages to subscribers as long they exist an removes disconnected subscribers
class PushCollection {
  private {
    struct Item {
      HttpPushLoop pushLoop;
      RoundRobin!Subscriber subscribers;
      MongoQueue!MQEnvelope queue;
      Timer[string] timers;
    }

    const ThrottleConfiguration config;
    Item[string] items;
    HTTPClientSettings settings;
    MongoDatabase db;
  }

  bool isRunning;

  ///
  this(MongoDatabase db, const ThrottleConfiguration config) {
    this.db = db;
    this.config = config;
    this.settings = new HTTPClientSettings();
    this.settings.defaultKeepAliveTimeout = 0.seconds;

    version(unittest) {} else runTask(&this.run);
  }

  void run() nothrow {
    this.isRunning = true;
    size_t delay;

    try {
      while(isRunning) {
        foreach(key, item; items) {
          if(item.pushLoop.isReady) {
            delay = 0;
            item.pushLoop.start;
            yield;
          }

          item.subscribers.removePenalized;
        }

        if(delay < 2000) {
          delay += 10;
        }

        sleep(delay.msecs);
      }
    } catch(Exception e) {
      error(e);
    }
  }

  ///
  bool handleData(string strUrl, Json data) nothrow {
    bool result;
    logDiagnostic("Sending POST HTTP request to `%s` subscriber: %s", strUrl, data);

    URL url;

    try {
      url = URL(strUrl);

      scope client = new HTTPClient;

      logDiagnostic("Connecting to http `%s` subscriber.", strUrl);
      client.connect(url.host, url.port, false, settings);

      scope(exit) {
        client.disconnect;
        logDiagnostic("Disconnected from http `%s` subscriber.", strUrl);
      }

      client.request((scope req) {
        if (url.localURI.length) {
          req.requestURL = url.localURI;
        }

        req.headers["Host"] = url.host;
        req.method = HTTPMethod.POST;
        req.writeJsonBody(data);
      }, (scope res) {
        logDiagnostic("Subscriber response: %s %s", res.statusCode, res.bodyReader.readAllUTF8());

        if(res.statusCode >= 200 && res.statusCode < 300) {
          result = true;
        }
      });
    } catch(Exception e) {
      logError("Failed to send request to: %s %s", strUrl, e.to!string);
    }

    try {
      Stats.instance.inc("Task", ["type": "http-push", "result": result ? "success" : "failure"]);
    } catch(Exception e) {
      logError("Failed to update http-push stats.");
    }

    return result;
  }

  /// Initializes a new channel
  void prepareChannel(string channel) @safe {
    if(channel in items || channel.length <= 2) {
      return;
    }

    debug assert(channel[0] != '/', "The channel must not start with `/`");

    logDiagnostic("Preparing `%s`", channel);
    auto queue = new MongoQueue!MQEnvelope(db[channel]);
    auto subscribers = new RoundRobin!Subscriber(this.config);
    items[channel] = Item(new HttpPushLoop(queue, subscribers, &this.handleData, channel), subscribers, queue);

    void onRemove(string id) @safe {
      logInfo("Removing subscriber `%s` on `%s`.", id, channel);

      if(channel in items && id in items[channel].timers) {
        items[channel].timers[id].stop;
        items[channel].timers.remove(id);
      }
    }

    subscribers.onRemove = &onRemove;
  }

  ///
  void start(string channel) @safe {
    logDiagnostic("Start pushing to `%s`...", channel);

    if(items[channel].pushLoop.isReady) {
      items[channel].pushLoop.start;
    } else {
      logDiagnostic("not ready for `%s`... isStarted=%s isBusy=%s hasData=%s", channel,
        items[channel].pushLoop.isStarted, items[channel].pushLoop.isBusy, items[channel].pushLoop.hasData);
    }
  }

  /// Pushes a messages to the queue and starts the channel loop
  void push(MQEnvelope value) @safe {
    prepareChannel(value.channel);

    items[value.channel].queue.put(value);
    diagnostic("%s has %s queued messages.", value.channel, items[value.channel].queue.length);

    this.start(value.channel);
  }

  ///
  void subscribe(string channel, Subscriber subscriber, size_t max) @safe {
    logInfo("Got a new subscriber `%s` on `%s` with `%s` max connections.", subscriber.id, channel, max);
    prepareChannel(channel);

    unsubscribe(channel, subscriber.id);
    auto index = items[channel].subscribers.addValue(subscriber);
    items[channel].subscribers.setMax(index, max);

    runTask({
      try {
        start(channel);
      } catch(Exception e) {
        error(e);
      }
    });

    items[channel].timers[subscriber.id] = setTimer(30.seconds, &subscriber.ping, true);
  }

  ///
  void unsubscribe(string channel, string id) @safe {
    if(channel !in items) {
      return;
    }

    items[channel].subscribers.removeValue(id);
  }
}
