/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.http_receiver;

import std.datetime;

import vibe.service.stats;

import gis_collective.hmq.configuration;
import gis_collective.hmq.envelope;

import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.server;

class HttpReceiver {

  private {
    static {
      size_t requestCount;
      SysTime lastReport;
    }

    void delegate(MQEnvelope) @safe onReceive;
  }

  this(void delegate(MQEnvelope) @safe onReceive) {
    this.onReceive = onReceive;

    Stats.instance.help("hmq_receive_message",
      "The number of received messages");
    Stats.instance.help("hmq_receive_duration",
      "The average duration in nanoseconds of how long it takes to receive a message");
    Stats.instance.help("hmq_receive_troughput",
      "The number of request that were processed in one second");

    lastReport = Clock.currTime;
  }

  void push(HTTPServerRequest req, HTTPServerResponse res) {
    string channel = req.requestPath.toString[1..$];
    requestCount++;

    if(Clock.currTime - lastReport > 1.seconds) {
      lastReport = Clock.currTime;
      Stats.instance.avg("hmq_receive_troughput", requestCount, ["channel": channel]);
      requestCount = 0;
    }

    auto begin = Clock.currTime;

    scope(exit) {
      auto diff = Clock.currTime - begin;
      Stats.instance.avg("hmq_receive_duration", diff.total!"nsecs", ["channel": channel]);
    }

    Stats.instance.inc("hmq_receive_message", ["channel": channel]);
    logDiagnostic("Received on `%s`: %s", channel, req.json);

    MQEnvelope envelope;
    envelope.channel = channel;
    envelope.time = Clock.currTime;
    envelope.data = req.json;

    if("uid" in req.json) {
      envelope.uid = req.json["uid"].to!string;
    }

    onReceive(envelope);
    res.writeBody(`{}`, 200, "application/json");
  }
}
