/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module gis_collective.hmq.round_robin;

import gis_collective.hmq.control.time_throttle;
import gis_collective.hmq.subscribers.base;

import std.exception;
import std.stdio;
import std.datetime;
import std.algorithm;
import std.array;
import std.conv;

version(unittest) {
  import gis_collective.hmq.subscribers.handler;
  import fluent.asserts;
}

/// A generic round robbin exception
class RoundRobinException : Exception {
  this(string msg = null, Throwable next = null) @safe
  {
    super(msg, next);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe
  {
    super(msg, file, line, next);
  }
}

///
class RoundRobin(T : Subscriber) {
  struct Item {
    size_t index;
    T value;
  }

  struct ItemMetaData {
    T value;
    TimeThrottle throttle;

    size_t load;
    size_t max;
  }

  private {
    immutable ThrottleConfiguration config;

    long lastIndex;
    ItemMetaData[] meta;
  }

  void delegate(string) @safe onRemove;

  ///
  this(const ThrottleConfiguration config) @safe {
    this.config = config;
  }

  ///
  this(T[] values, ThrottleConfiguration config = ThrottleConfiguration.init) @safe {
    this(config);
    addValues(values);
    this.lastIndex = this.meta.length - 1;
  }

  /// Operator overload for []
  T opIndex(size_t index) @safe nothrow {
    return meta[index].value;
  }

  /// Add a list of values to the container
  void addValues(T[] values) @safe {
    foreach(value; values) {
      addValue(value);
    }
  }

  /// Add a value and it returns the index
  size_t addValue(T value) @safe {
    size_t index = this.meta.length;
    bool found;

    foreach(i, item; meta) {
      if(item.value.id == value.id) {
        index = i;
        found = true;
        break;
      }
    }

    if(!found) {
      this.meta ~= ItemMetaData(value, new TimeThrottle(config));
    }

    this.meta[index].max = size_t.max;
    this.meta[index].throttle.resetTime;

    return index;
  }

  /// Remove a value by name
  void removeValue(string value) @safe {
    foreach(index, item; meta) {
      if(item.value.id == value) {
        this.meta = this.meta[0..index] ~ this.meta[index+1 .. $];

        if(this.onRemove !is null) {
          this.onRemove(value);
        }
      }
    }
  }

  /// Set the max number of jobs that the item supports
  void setMax(size_t index, size_t value) @safe {
    if(index >= this.meta.length) {
      return;
    }

    this.meta[index].max = value;
  }

  /// Get the smallest load value
  long minLoad() {
    size_t min = size_t.max;
    auto now = Clock.currTime;

    foreach(i; 0..meta.length) {
      if(min > meta[i].load && !meta[i].throttle.isFaulty) {
        min = meta[i].load;
      }
    }

    return min;
  }

  /// Get the next least used value
  Item next() {
    enforce!RoundRobinException(meta.length > 0, "The list is empty.");

    immutable min = this.minLoad;
    immutable now = Clock.currTime;

    foreach(i; 0..meta.length) {
      const index = (lastIndex + i + 1) % meta.length;

      if(meta[index].load != min) {
        continue;
      }

      if(meta[index].load >= this.meta[index].max) {
        continue;
      }

      if(this.meta[index].throttle.isFaulty) {
        continue;
      }

      lastIndex = index;
      break;
    }

    return Item(lastIndex, meta[lastIndex].value);
  }

  /// Mark an item as busy
  void reserve(long index) {
    if(index >= this.meta.length) {
      return;
    }

    meta[index].load++;
  }

  /// Penalize an item for bad behaviour
  void penalize(long index) {
    if(index >= this.meta.length) {
      return;
    }

    meta[index].throttle.penalize;
  }

  /// Reward a item when it worked well
  void reward(long index) {
    if(index >= this.meta.length) {
      return;
    }

    this.meta[index].throttle.reset;
  }

  /// Get the item usage by index
  size_t usage(long index) @safe nothrow {
    if(index >= this.meta.length) {
      return size_t.max;
    }

    return meta[index].load;
  }

  /// Free a resource
  void free(long index) @safe {
    if(index >= this.meta.length) {
      return;
    }

    enforce(meta[index].load > 0, "You can't free a free resource!");
    meta[index].load--;
  }

  /// Check if a value is busy
  bool isBusy() @safe nothrow {
    immutable now = Clock.currTime;

    foreach(i; 0..meta.length) {
      if(meta[i].load < meta[i].max && !this.meta[i].throttle.isFaulty) {
        return false;
      }
    }

    return true;
  }

  /// Remove all penalized items
  void removePenalized() {
    auto maxPenalizations = config.penalizationTotalMs / config.penalizationMs;

    if(this.onRemove !is null) {
      foreach(item; meta.filter!(a => a.throttle.penalizations >= maxPenalizations || a.value.isFaulty)) {
        this.onRemove(item.value.id);
      }
    }

    meta = meta.filter!(a => a.throttle.penalizations < maxPenalizations && !a.value.isFaulty).array;
  }
}

/// It should add and remove values
unittest {
  import std.algorithm;
  import std.array;

  auto rr = new RoundRobin!HandlerSubscriber([]);

  rr.addValue(new HandlerSubscriber("1"));
  rr.addValue(new HandlerSubscriber("2"));

  rr.meta.map!(a => a.value.id).array.should.equal(["1", "2"]);

  rr.removeValue("1");
  rr.meta.map!(a => a.value.id).array.should.equal(["2"]);
  rr.removeValue("2");
  rr.meta.map!(a => a.value.id).array.should.equal([]);
  rr.removeValue("2");
  rr.meta.map!(a => a.value.id).array.should.equal([]);
}

/// It should iterate trough each value
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.isBusy.should.equal(true);

  ({
    rr.next();
  }).should.throwException!RoundRobinException.msg.should.equal("The list is empty.");
}

/// It should iterate trough each value
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.next.value.id.should.equal("0");
  rr.next.value.id.should.equal("1");
  rr.next.value.id.should.equal("2");
  rr.next.value.id.should.equal("0");
}

/// It should iterate trough each index
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.next.index.should.equal(0);
  rr.next.index.should.equal(1);
  rr.next.index.should.equal(2);
  rr.next.index.should.equal(0);
}

/// It should iterate trough less loaded indexes
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.reserve(0);
  rr.next.value.id.should.equal("1");
  rr.next.value.id.should.equal("2");
  rr.next.value.id.should.equal("1");
}

/// It should iterate freed items
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.reserve(0);
  rr.next.value.id.should.equal("1");
  rr.next.value.id.should.equal("2");
  rr.free(0);
  rr.next.value.id.should.equal("0");
}

/// It should iterate freed items if they did not reach the max usage
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.reserve(0);
  rr.reserve(1);
  rr.reserve(2);
  rr.setMax(1, 0);

  rr.next.value.id.should.equal("0");
  rr.next.value.id.should.equal("2");
  rr.next.value.id.should.equal("0");
}

/// It should mark it busy when all items are used
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.isBusy.should.equal(false);

  rr.reserve(0);
  rr.reserve(1);
  rr.reserve(2);
  rr.setMax(0, 0);
  rr.setMax(1, 0);
  rr.setMax(2, 0);

  rr.isBusy.should.equal(true);
}

/// it should skip the first item if it is penalized
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.isBusy.should.equal(false);

  rr.penalize(0);
  rr.next.value.id.should.equal("1");
}

/// it should not apply penalization if max time is 0
unittest {
  ThrottleConfiguration config;
  config.penalizationTotalMs = 0;

  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")], config);

  rr.isBusy.should.equal(false);

  rr.penalize(0);
  rr.next.value.id.should.equal("0");
}

/// it should ignore penalization busy state on getting the next item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.isBusy.should.equal(false);

  rr.penalize(0);
  rr.reserve(1);
  rr.reserve(2);
  rr.isBusy.should.equal(false);
  rr.next.value.id.should.equal("1");
}

/// it should remove penalized items if the reach the penalizationTotalMs
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  foreach(i; 0..20) {
    rr.penalize(0);
  }

  rr.removePenalized;
  rr.meta.length.should.equal(2);
}

/// it should trigger on remove for penalized items
unittest {
  string[] removed;

  void onRemove(string id) @safe nothrow {
    removed ~= id;
  }

  auto rr = new RoundRobin!HandlerSubscriber([
    new HandlerSubscriber("0"),
    new HandlerSubscriber("1"),
    new HandlerSubscriber("2")]);

  rr.onRemove = &onRemove;

  foreach(i; 0..20) {
    rr.penalize(0);
  }

  rr.removePenalized;
  rr.meta.length.should.equal(2);

  removed.should.equal(["0"]);
}

/// it should remove faulty items
unittest {
  auto item = new HandlerSubscriber("1");
  item.isFaulty = true;

  auto rr = new RoundRobin!HandlerSubscriber([ item ]);

  rr.removePenalized;
  rr.meta.length.should.equal(0);
}

/// it should trigger on remove for faulty items
unittest {
  string[] removed;

  auto item = new HandlerSubscriber("1");
  item.isFaulty = true;

  void onRemove(string id) @safe nothrow {
    removed ~= id;
  }

  auto rr = new RoundRobin!HandlerSubscriber([ item ]);
  rr.onRemove = &onRemove;

  rr.removePenalized;
  rr.removePenalized;

  removed.should.equal(["1"]);
}

/// it should be able to set the max for a missing item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.setMax(1, 1);
}

/// it should be able to reserve a missing item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.reserve(1);
}

/// it should be able to penalise a missing item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.penalize(1);
}

/// it should be able to reward a removed item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.reward(1);
}

/// it should be able to free a removed item
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.free(1);
}

/// the usage of a missing item should be size_t max
unittest {
  auto rr = new RoundRobin!HandlerSubscriber([]);
  rr.usage(1).should.equal(size_t.max);
}