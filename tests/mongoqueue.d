/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.mongoqueue;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import core.memory;

import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.http_receiver;
import gis_collective.hmq.envelope;

import vibe.data.json;
import vibe.db.mongo.mongo;

alias suite = Spec!({
  MongoCollection collection;
  MongoClient client;

  describe("mongo queue", {

    beforeEach({
      client = connectMongoDB("mongo");
      collection = client.getCollection("test.queue");

      try {
        collection.drop;
      } catch(Exception) {}
    });

    afterEach({
      GC.collect;
      GC.minimize;
      client.cleanupConnections;
    });

    it("should store all values", {
      auto queue = new MongoQueue!MQEnvelope(collection);
      auto envelope = MQEnvelope();
      queue.put(envelope);
      queue.storeAll;

      queue.length.should.equal(0);
      collection.countDocuments(Bson.emptyObject).should.equal(1);
    });

    it("should store only unique values by uid", {
      auto queue = new MongoQueue!MQEnvelope(collection);
      auto envelope = MQEnvelope();
      envelope.uid = "1";

      queue.put(envelope);
      queue.put(envelope);
      queue.storeAll;

      queue.length.should.equal(0);
      collection.countDocuments(Bson.emptyObject).should.equal(1);
    });

    it("should ignore already stored ids", {
      auto queue = new MongoQueue!MQEnvelope(collection);
      auto envelope = MQEnvelope();
      envelope.uid = "1";

      queue.put(envelope);
      queue.storeAll;
      queue.put(envelope);

      queue.length.should.equal(0);
      collection.countDocuments(Bson.emptyObject).should.equal(1);
    });
  });
});
