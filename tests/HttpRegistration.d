/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module submodules.hmq.tests.HttpRegistration;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;
import core.memory;

import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.http_receiver;
import gis_collective.hmq.envelope;
import gis_collective.hmq.broadcast.http;

import vibe.data.json;
import vibe.db.mongo.mongo;

alias suite = Spec!({
  BroadcastConfig config;
  HttpRegistration httpRegistration;

  beforeEach({
    config.localUrl = "http://localhost";
    httpRegistration = new HttpRegistration(config);
  });

  it("adds the channel to a list on register", {

    auto httpRegistration = new HttpRegistration(config);

    httpRegistration.register("channel");
    httpRegistration.pendingRegistrations.should.equal(["channel"]);
  });

  it("generates a registrationMessage with all the registered channels", {
    httpRegistration.register("channel1");
    httpRegistration.register("channel2");
    httpRegistration.register("channel3");
    expect(httpRegistration.registrationMessage).to.equal(`{
      "channelList": ["channel1", "channel2", "channel3"],
      "max": 10,
      "url": "http://localhost"
    }`.parseJsonString);
  });
});
