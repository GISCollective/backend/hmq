/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.subscriber;

import tests.fixtures;

import gis_collective.hmq.api;
import gis_collective.hmq.configuration;
import gis_collective.hmq.envelope;
import gis_collective.hmq.http_subscriber;
import gis_collective.hmq.subscribers.base;

import vibe.http.router;
import vibe.data.json;

alias suite = Spec!({
  URLRouter router;

  describe("with a task http subscriber", {

    beforeEach({
      router = new URLRouter;
    });

    it("should accept a valid subscriber" , {
      bool called;
      string channel;
      Subscriber subscriber;
      size_t max;

      void onReceive(MQEnvelope) {}
      void onUnsubscribe(string, string) {}
      void onSubscribe(string c, Subscriber s, size_t m) {
        called = true;
        channel = c;
        subscriber = s;
        max = m;
      }

      scope httpSubscribe = new HttpSubscribe(&onSubscribe, &onUnsubscribe, &onReceive);
      Json data = `{"channel": "/", "url": "http://", "max": 10}`.parseJsonString;

      router.put("/subscribe", &httpSubscribe.subscribe);

      router
        .request
        .put("/subscribe")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          response.bodyJson.should.equal(`{}`.parseJsonString);
        });

      called.should.equal(true);
      channel.should.equal("/");
      subscriber.id.should.equal("http:///");
      max.should.equal(10);
    });


    it("should accept a valid unsubscriber" , {
      bool called;

      void onReceive(MQEnvelope) {}
      void onUnsubscribe(string channel, string url) {
        channel.should.equal("/");
        url.should.equal("http://");
        called = true;
      }
      void onSubscribe(string, Subscriber, size_t) {
      }

      scope httpSubscribe = new HttpSubscribe(&onSubscribe, &onUnsubscribe, &onReceive);
      Json data = `{"channel": "/", "url": "http://"}`.parseJsonString;

      router.put("/unsubscribe", &httpSubscribe.unsubscribe);

      router
        .request
        .put("/unsubscribe")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          response.bodyJson.should.equal(`{}`.parseJsonString);
        });

      called.should.equal(true);
    });

    it("should not accept invalid jsons" , {
      scope httpSubscribe = new HttpSubscribe(null, null, null);

      router.put("/subscribe", &httpSubscribe.subscribe);

      router
        .request
        .put("/subscribe")
        .send("asd")
        .expectStatusCode(400)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "Expected JSON object, got undefined",
              "status": 400,
              "title": "Parse error"
            }]
          }`.parseJsonString);
        });
    });

    it("should not accept missing channel key" , {
      scope httpSubscribe = new HttpSubscribe(null, null, null);

      router.put("/subscribe", &httpSubscribe.subscribe);
      Json data = `{"url": "http://", "max": 10}`.parseJsonString;

      router
        .request
        .put("/subscribe")
        .send(data)
        .expectStatusCode(400)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          response.bodyJson.should.equal(`{
            "errors": [{
              "description": "You must provide a 'channel' or 'channelList'.",
              "status": 400,
              "title": "Invalid channel"
            }]
          }`.parseJsonString);
        });
    });
  });
});
