/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.push;

import tests.fixtures;

import vibe.core.core;

import std.stdio;
import std.algorithm;
import std.array;
import std.datetime;

import core.time : msecs;

import gis_collective.hmq.http_push_loop;
import gis_collective.hmq.api;
import gis_collective.hmq.configuration;
import gis_collective.hmq.envelope;
import gis_collective.hmq.round_robin;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.subscribers.base;
import gis_collective.hmq.subscribers.handler;
import gis_collective.hmq.control.time_throttle;

import vibe.http.router;
import vibe.data.json;

alias suite = Spec!({
  URLRouter router;

  describe("with a push service", {
    HttpPushLoop pushLoop;

    describe("with a single subscriber", {
      beforeEach({
        router = new URLRouter;
      });

      it("should cycle trough the available subscribers", {
        auto queue = new MemoryQueue!MQEnvelope;

        queue.put(`{ "channel": "", "data": { "field": 1 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 2 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 3 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 4 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 5 } }`.parseJsonString.deserializeJson!MQEnvelope);

        size_t index;
        string[] urlList;
        bool handler(string url, Json data) nothrow {
          try {
            urlList ~= url;
            index++;
            yield;
          } catch(Exception) { }

          return true;
        }

        auto subscribers = new RoundRobin!Subscriber([
          new HandlerSubscriber("http://otherhost", &handler),
          new HandlerSubscriber("http://localhost", &handler)]);

        subscribers.setMax(0, 1);
        subscribers.setMax(1, 2);

        pushLoop = new HttpPushLoop(queue, subscribers, &handler, "test");

        pushLoop.start();
        runEventLoopOnce();
        urlList.should.equal([ "http://otherhost", "http://localhost", "http://localhost" ]);
        index.should.equal(3);

        urlList = [];
        runEventLoopOnce();
        pushLoop.start();
        runEventLoopOnce();

        urlList.should.equal([ "http://otherhost", "http://localhost" ]);
        index.should.equal(5);
      });


      it("should penalize an item on failure", {
        auto queue = new MemoryQueue!MQEnvelope;

        queue.put(`{ "channel": "", "data": { "field": 1 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 2 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 3 } }`.parseJsonString.deserializeJson!MQEnvelope);
        queue.put(`{ "channel": "", "data": { "field": 4 } }`.parseJsonString.deserializeJson!MQEnvelope);

        size_t index;
        string[] urlList;

        bool handler(string url, Json data) nothrow {
          urlList ~= url;
          index++;
          return false;
        }

        auto configuration = ThrottleConfiguration(10000);
        auto subscribers = new RoundRobin!Subscriber([
          new HandlerSubscriber("http://otherhost", &handler),
          new HandlerSubscriber("http://localhost", &handler)], configuration);
        subscribers.setMax(0, 1);
        subscribers.setMax(1, 2);

        pushLoop = new HttpPushLoop(queue, subscribers, &handler, "test");

        pushLoop.start();
        runEventLoopOnce();

        urlList.should.equal(["http://otherhost", "http://localhost", "http://localhost" ]);

        index.should.equal(3);

        urlList = [];
        runEventLoopOnce();
        pushLoop.start();
        runEventLoopOnce();

        urlList.should.equal([ ]);
        index.should.equal(3);
      });
    });
  });
});
