/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.fixtures;

public import fluent.asserts;
public import trial.discovery.spec;

import vibe.core.log;

shared static this() {
  foreach(logger; getLoggers()) {
    deregisterLogger(logger);
  }
}
