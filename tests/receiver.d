/**
  Copyright: © 2015-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module tests.http;

import tests.fixtures;

import std.algorithm;
import std.array;
import std.datetime;

import gis_collective.hmq.http_receiver;
import gis_collective.hmq.envelope;

import vibe.http.router;
import vibe.data.json;

alias suite = Spec!({
  URLRouter router;

  describe("with a task http service", {

    beforeEach({
      router = new URLRouter;
    });

    it("should accept a json value" , {
      Json data = `{"key": "some value"}`.parseJsonString;

      bool received;
      void receive(MQEnvelope value) {
        received = true;

        value.channel.should.equal("test");
        value.time.toUnixTime.should.be.approximately(Clock.currTime.toUnixTime, 1);
        value.data.should.equal(data);
        value.uid.should.equal("");
      }

      auto receiver = new HttpReceiver(&receive);
      router.post("*", &receiver.push);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          received.should.equal(true);
        });
    });


    it("should add the uid when it is sent" , {
      Json data = `{"key": "some value", "uid": "uid"}`.parseJsonString;

      bool received;
      void receive(MQEnvelope value) {
        received = true;

        value.channel.should.equal("test");
        value.time.toUnixTime.should.be.approximately(Clock.currTime.toUnixTime, 1);
        value.data.should.equal(data);
        value.uid.should.equal("uid");
      }

      auto receiver = new HttpReceiver(&receive);
      router.post("*", &receiver.push);

      router
        .request
        .post("/test")
        .send(data)
        .expectStatusCode(200)
        .expectHeader("Content-Type", "application/json")
        .end((Response response) => () {
          received.should.equal(true);
        });
    });
  });
});
