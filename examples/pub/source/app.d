import gis_collective.hmq.broadcast.http;
import gis_collective.hmq.broadcast.ws;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.lifecycle;

import vibe.core.log;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;

int main() {

  setLogLevel(LogLevel.debug_);

  URLRouter router;

  BroadcastConfig config;
  config.hmqUrl = "http://127.0.0.1:5000/";
  config.localUrl = "http://127.0.0.1:6000/";
  config.router = router;

  auto httpBroadcast = new HttpBroadcast(config);
  auto wsBroadcast = new WsBroadcast(config);

  runTask({
    long i;
    while(true) {
      if(!httpBroadcast.isFaulty) {
        httpBroadcast.push("http_test", Json(i));
      }
      wsBroadcast.push("ws_test", Json(i));
      i++;
      yield;
    }
  });

  scope(exit) LifeCycle.instance.shutdown;

  return runEventLoop();
}
