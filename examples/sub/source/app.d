import gis_collective.hmq.broadcast.http;
import gis_collective.hmq.broadcast.ws;
import gis_collective.hmq.broadcast.base;

import std.functional;

import vibe.core.log;
import vibe.core.core;
import vibe.data.json;
import vibe.http.router;
import vibe.http.server;

import std.stdio;

void handleHttpMessage(const(Json) value) @trusted {
  writeln("http ", value);
}

void handleWsMessage(const(Json) value) @trusted {
  writeln("ws ", value);
}

int main() {
  setLogLevel(LogLevel.diagnostic);

  auto router = new URLRouter;

  BroadcastConfig config;
  config.hmqUrl = "http://127.0.0.1:5000/";
  config.localUrl = "http://127.0.0.1:6000/";
  config.router = router;

  auto httpBroadcast = new HttpBroadcast(config);
  auto wsBroadcast = new WsBroadcast(config);

  runTask({
    httpBroadcast.register("http_test", toDelegate(&handleHttpMessage));
    wsBroadcast.register("ws_test", toDelegate(&handleWsMessage));
  });

  auto httpSettings = new HTTPServerSettings;
  httpSettings.port = 6000;

  listenHTTP(httpSettings, router);

  return runEventLoop();
}
